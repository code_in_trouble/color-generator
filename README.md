# Color Generator

This app generates 5 random colors using the RGB, hex, and CMYK formats. A new palette can be generated using the refresh button and colors can be checked against black or white backgrounds by using the theme switcher button.

Hosted at: [Color Generator](https://rogue-cyborg.gitlab.io/color-generator)

## Preview
![Preview Image](https://gitlab.com/rogue-cyborg/color-generator/blob/master/preview.png)
